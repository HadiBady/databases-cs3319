<?php
$query = "SELECT fname, lname, petname FROM owner, pet WHERE pet.ownerid = owner.ownerid";
$result = mysqli_query($connection,$query);
if (!$result) {
    die("databases query failed.");
}
echo "<ol>";
while ($row = mysqli_fetch_assoc($result)) {
     echo "<li>";
     echo $row["fname"]." ".$row["lname"]." -- ".$row["petname"]."</li>";
}
mysqli_free_result($result);
echo"</ol>";
?>
