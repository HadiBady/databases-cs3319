<?php
	$whichMus = $_POST["pickamuseum"]; //get selected museum value from the form
	$query = "SELECT * FROM workofart WHERE whichmus = ".$whichMus;  //query to get all the works of art for the selected museum

	$result = mysqli_query($connection, $query);
	if (!$result) {
		die("databases query on art pieces failed.");
	}

	echo mysqli_fect_assoc($result);

	echo "<ul>";//put the artwork in an unordered bulleted list

	while ($row = mysqli_fect_assoc($result)){
		echo "<li>".$row["artname"]." BY ".$row["artist"];
	}
	echo "</ul>";

	echo "</ul>";//end the bulleted list
	mysqli_free_result($result);

?>
